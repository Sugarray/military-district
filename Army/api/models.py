from django.db import models
from jsonfield import JSONField


class Rank(models.Model):

    class Meta:
        verbose_name_plural = "rank"

    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class Dislocation(models.Model):

    class Meta:
        verbose_name_plural = "dislocation"

    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class ItemType(models.Model):

    class Meta:
        verbose_name_plural = "dislocation"

    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class BuildingType(models.Model):

    class Meta:
        verbose_name_plural = "building type"

    name = models.CharField(max_length=100, unique=True)

    def __unicode__(self):
        return self.name


class MilitarySpeciality(models.Model):

    class Meta:
        verbose_name_plural = "military speciality"

    name = models.CharField(max_length=100, null=True, blank=True)
    speciality_code = models.IntegerField(primary_key=True)

    def __unicode__(self):
        return '{0} - {1}'.format(self.speciality_code, self.name)


class Military(models.Model):

    class Meta:
        verbose_name_plural = "military"

    first_name = models.CharField(max_length=100, null=False)
    last_name = models.CharField(max_length=100, null=False)
    passport = models.IntegerField(null=False, unique=True)
    rank = models.ForeignKey(Rank, to_field='name')
    details = JSONField(null=True, blank=True)
    speciality = models.ManyToManyField(MilitarySpeciality)

    compartment_id = models.ForeignKey('Compartment', to_field='compartment_id',
                                       null=True, blank=True)

    def __unicode__(self):
        return 'FName:{0} LName:{1} Passport:{2}'.format(self.first_name,
                                                         self.last_name,
                                                         self.passport)


class Compartment(models.Model):

    class Meta:
        verbose_name_plural = "compartment"

    compartment_id = models.IntegerField(unique=True)
    compartment_name = models.CharField(max_length=100, null=True, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)
    troop_id = models.ForeignKey('Troop', to_field='troop_id', null=True,
                                 blank=True)

    def __unicode__(self):
        return "Compartment name:{0}  id:{1}".format(self.compartment_name,
                                                     self.compartment_id)


class Troop(models.Model):

    class Meta:
        verbose_name_plural = "troop"

    troop_id = models.IntegerField(unique=True)
    troop_name = models.CharField(max_length=100, null=True, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)
    company_id = models.ForeignKey('Company', to_field='company_id', null=True,
                                   blank=True)

    def __unicode__(self):
        return "Troop name:{0}  id:{1}".format(self.troop_name, self.troop_id)


class Company(models.Model):

    class Meta:
        verbose_name_plural = "company"

    company_id = models.IntegerField(unique=True)
    company_name = models.CharField(max_length=100, null=True, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)
    base_id = models.ForeignKey('MilitaryBase',
                                to_field='base_id', null=True)

    def __unicode__(self):
        return "Company name:{0}  id:{1}".format(self.company_name,
                                                 self.company_id)


class MilitaryBase(models.Model):

    class Meta:
        verbose_name_plural = "military base"

    base_id = models.IntegerField(unique=True)
    base_name = models.CharField(max_length=100, blank=True)
    dislocation = models.ForeignKey(Dislocation, to_field='name', null=True,
                                    blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)

    inventory = models.ManyToManyField('Item', through='MilitaryBaseInventory',
                                       blank=True)

    division_id = models.ForeignKey('Division', to_field='division_id',
                                    null=True, blank=True)

    def __unicode__(self):
        return "Unit name:{0}  id:{1}  Dislocation:{2}".format(
            self.base_name,
            self.base_id,
            self.dislocation)


class Division(models.Model):

    class Meta:
        verbose_name_plural = "division"

    division_id = models.IntegerField(unique=True)
    division_name = models.CharField(max_length=100, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)

    corps_id = models.ForeignKey('Corps', to_field='corps_id', null=True,
                                 blank=True)

    def __unicode__(self):
        return "Division name:{0}  id:{1}".format(self.division_name,
                                                  self.division_id)


class Corps(models.Model):

    class Meta:
        verbose_name_plural = "corps"

    corps_id = models.IntegerField(unique=True)
    corps_name = models.CharField(max_length=100, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)

    army_id = models.ForeignKey('Army', to_field='army_id', null=True,
                                blank=True)

    def __unicode__(self):
        return "Corps name:{0}  id:{1}".format(self.corps_name,
                                               self.corps_id)


class Army(models.Model):

    class Meta:
        verbose_name_plural = "army"

    army_id = models.IntegerField(unique=True)
    army_name = models.CharField(max_length=100, blank=True)
    commander = models.OneToOneField(Military, to_field='passport',
                                     null=True, blank=True)

    def __unicode__(self):
        return "main name:{0}  id:{1}".format(self.army_name,
                                              self.army_id)


class Item(models.Model):

    class Meta:
        verbose_name_plural = "item"

    name = models.CharField(max_length=100, blank=True)
    type = models.ForeignKey(ItemType, to_field='name', blank=True)
    item_number = models.IntegerField(unique=True)
    details = JSONField(blank=True)

    def __unicode__(self):
        return "Name:{0}  Type:{1}".format(self.name,
                                           self.type)


class Building(models.Model):

    class Meta:
        verbose_name_plural = "building"

    name = models.CharField(max_length=100, blank=True)
    type = models.ForeignKey(BuildingType, to_field='name', blank=True)
    area = models.FloatField()

    military_base = models.ForeignKey(MilitaryBase, blank=True,
                                      to_field='base_id')


class MilitaryBaseInventory(models.Model):

    class Meta:
        verbose_name_plural = "military base inventory"

    base_id = models.ForeignKey(MilitaryBase, to_field='base_id')
    inventory = models.ForeignKey(Item)
    inventory_count = models.IntegerField(blank=True)
