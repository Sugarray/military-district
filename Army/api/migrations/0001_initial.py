# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Army',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('army_id', models.IntegerField(unique=True)),
                ('army_name', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'verbose_name_plural': 'army',
            },
        ),
        migrations.CreateModel(
            name='Building',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('area', models.FloatField()),
            ],
            options={
                'verbose_name_plural': 'building',
            },
        ),
        migrations.CreateModel(
            name='BuildingType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'building type',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('company_id', models.IntegerField(unique=True)),
                ('company_name', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'company',
            },
        ),
        migrations.CreateModel(
            name='Compartment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('compartment_id', models.IntegerField(unique=True)),
                ('compartment_name', models.CharField(max_length=100, null=True, blank=True)),
            ],
            options={
                'verbose_name_plural': 'compartment',
            },
        ),
        migrations.CreateModel(
            name='Corps',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('corps_id', models.IntegerField(unique=True)),
                ('corps_name', models.CharField(max_length=100, blank=True)),
                ('army_id', models.ForeignKey(to_field=b'army_id', blank=True, to='api.Army', null=True)),
            ],
            options={
                'verbose_name_plural': 'corps',
            },
        ),
        migrations.CreateModel(
            name='Dislocation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'dislocation',
            },
        ),
        migrations.CreateModel(
            name='Division',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('division_id', models.IntegerField(unique=True)),
                ('division_name', models.CharField(max_length=100, blank=True)),
            ],
            options={
                'verbose_name_plural': 'division',
            },
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('item_number', models.IntegerField(unique=True)),
                ('details', jsonfield.fields.JSONField(blank=True)),
            ],
            options={
                'verbose_name_plural': 'item',
            },
        ),
        migrations.CreateModel(
            name='ItemType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'dislocation',
            },
        ),
        migrations.CreateModel(
            name='Military',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=100)),
                ('last_name', models.CharField(max_length=100)),
                ('passport', models.IntegerField(unique=True)),
                ('details', jsonfield.fields.JSONField(null=True, blank=True)),
                ('compartment_id', models.ForeignKey(to_field=b'compartment_id', blank=True, to='api.Compartment', null=True)),
            ],
            options={
                'verbose_name_plural': 'military',
            },
        ),
        migrations.CreateModel(
            name='MilitaryBase',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_id', models.IntegerField(unique=True)),
                ('base_name', models.CharField(max_length=100, blank=True)),
                ('commander', models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military')),
                ('dislocation', models.ForeignKey(to_field=b'name', blank=True, to='api.Dislocation', null=True)),
                ('division_id', models.ForeignKey(to_field=b'division_id', blank=True, to='api.Division', null=True)),
            ],
            options={
                'verbose_name_plural': 'military base',
            },
        ),
        migrations.CreateModel(
            name='MilitaryBaseInventory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('inventory_count', models.IntegerField(blank=True)),
                ('base_id', models.ForeignKey(to='api.MilitaryBase', to_field=b'base_id')),
                ('inventory', models.ForeignKey(to='api.Item')),
            ],
            options={
                'verbose_name_plural': 'military base inventory',
            },
        ),
        migrations.CreateModel(
            name='MilitarySpeciality',
            fields=[
                ('name', models.CharField(max_length=100, null=True, blank=True)),
                ('speciality_code', models.IntegerField(serialize=False, primary_key=True)),
            ],
            options={
                'verbose_name_plural': 'military speciality',
            },
        ),
        migrations.CreateModel(
            name='Rank',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'verbose_name_plural': 'rank',
            },
        ),
        migrations.CreateModel(
            name='Troop',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('troop_id', models.IntegerField(unique=True)),
                ('troop_name', models.CharField(max_length=100, null=True, blank=True)),
                ('commander', models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military')),
                ('company_id', models.ForeignKey(to_field=b'company_id', blank=True, to='api.Company', null=True)),
            ],
            options={
                'verbose_name_plural': 'troop',
            },
        ),
        migrations.AddField(
            model_name='militarybase',
            name='inventory',
            field=models.ManyToManyField(to='api.Item', through='api.MilitaryBaseInventory', blank=True),
        ),
        migrations.AddField(
            model_name='military',
            name='rank',
            field=models.ForeignKey(to='api.Rank', to_field=b'name'),
        ),
        migrations.AddField(
            model_name='military',
            name='speciality',
            field=models.ManyToManyField(to='api.MilitarySpeciality'),
        ),
        migrations.AddField(
            model_name='item',
            name='type',
            field=models.ForeignKey(to='api.ItemType', blank=True, to_field=b'name'),
        ),
        migrations.AddField(
            model_name='division',
            name='commander',
            field=models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military'),
        ),
        migrations.AddField(
            model_name='division',
            name='corps_id',
            field=models.ForeignKey(to_field=b'corps_id', blank=True, to='api.Corps', null=True),
        ),
        migrations.AddField(
            model_name='corps',
            name='commander',
            field=models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military'),
        ),
        migrations.AddField(
            model_name='compartment',
            name='commander',
            field=models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military'),
        ),
        migrations.AddField(
            model_name='compartment',
            name='troop_id',
            field=models.ForeignKey(to_field=b'troop_id', blank=True, to='api.Troop', null=True),
        ),
        migrations.AddField(
            model_name='company',
            name='base_id',
            field=models.ForeignKey(to='api.MilitaryBase', to_field=b'base_id', null=True),
        ),
        migrations.AddField(
            model_name='company',
            name='commander',
            field=models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military'),
        ),
        migrations.AddField(
            model_name='building',
            name='military_base',
            field=models.ForeignKey(to='api.MilitaryBase', blank=True, to_field=b'base_id'),
        ),
        migrations.AddField(
            model_name='building',
            name='type',
            field=models.ForeignKey(to='api.BuildingType', blank=True, to_field=b'name'),
        ),
        migrations.AddField(
            model_name='army',
            name='commander',
            field=models.OneToOneField(null=True, to_field=b'passport', blank=True, to='api.Military'),
        ),
    ]
